# E3. Exercici 3. Xarxa

## Introducció

Sempre cal anar a la documentació oficial de la distribució que estem configurant.

## Continguts

- La documentació de Fedora la podeu trobar a: https://docs.fedoraproject.org/en-US/Fedora/20/html/Networking_Guide/index.html
- A Debian tenim: https://wiki.debian.org/NetworkConfiguration

## Entrega

1. **En grups de tres poseu-vos una adreça 172.16.0.X/24 (on la X és un número correlatiu diferent per cadascun i que ha de coincidir amb l'últim byte de l'adreça que teniu actualment per tal de no solapar-vos) a l'interfície de xarxa fent servir la comanda ip address. Comproveu que podeu fer ping entre vosaltres.**

2. **Elimineu aquesta adreça de l'interfície i indiqueu la comanda que heu fet servir per a fer-ho**

3. **Configureu ara la mateixa adreça per tal que quedi permanent en reiniciar l'ordinador al fitxer ifcfg-... corresponent del directori /etc/sysconfig/network-scripts. Reinicieu i comproveu que la manté. Copieu aquí el contingut del fitxer que heu fet.**

4. **Torneu a deixar el fitxer amb la configuració d'adreça dinàmica (DHCP) original i reinicieu. Copieu aquí el contingut del fitxer que heu fet.**

5. **Com faríem el pas 3 (configurar de manera permanent una adreça ip) en un sistema basat en Debian? Podeu fer servir una màquina virtual debian 9 de Isard.**

6. **On podem veure en quin fil de CPU s'estan servint les interrupcions que demana la nostra interfície de xarxa? Comproveu-ho i poseu la comanda i la sortida**

7. **Què és el servei irqbalance i per a què serveix?**

8. **Què és el servei tuned i per a què serveix?**

9. **Instal.leu el servei tuned i configureu el vostre sistema operatiu per tal que estigui optimitzat per a treballar com un escriptori (Desktop). Indiqueu les comandes que heu fet servir.**

10. **És possible aplicar dos perfils amb tuned-adm a l'hora? Proveu-ho**
