# E3. Exercici 2. JournalD

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. **Analitzeu els logs de l'última arrencada del vostre sistema operatiu i indiqueu:**
   - Ordre per mostrar el log:
   - Ordre per veure només els errors amb prioritat d'error des de l'última arrencada:
   - Ordre per veure només els errors amb prioritat d'emergència des de l'última arrencada:
   - Ordre per veure només els errors amb prioritat d'alerta des de l'última arrencada:
2. **Com podem veure el log del nostre mini-servidor web que arrenquem amb el systemd?**
   - Ordre:
   - Sortida de l'ordre anterior:
3. **Com podem veure els accessos que es fan al nostre servidor en temps real des del log de journal? **
   - Ordre:

4. **Feu un cron que comprovi cada minut si el servei sshd està corrent o no al nostre sistema operatiu.**
   - Ordre i/o fitxers per a dur a terme el cron:
5. **Feu ara que aquest cron miri si el servei sshd està corrent i el pari i enviï un missatge d'error de nivell emergència al journal..**
   - Ordre i/o fitxers per a dur a terme el cron:
   - Proveu d'iniciar i aturar aquest servei i mostreu l'ordre amb la que podem monitoritzar si està o no funcionant (mostra missatges al journal)